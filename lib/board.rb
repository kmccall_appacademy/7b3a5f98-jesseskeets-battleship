# class Board
#
#
#   def self.default_grid
#     Array.new(10) { Array.new(10) }
#   end
#
#
#   attr_reader :grid
#
#   def initialize(grid = self.class.default_grid, random = false)
#     @grid = grid
#     randomize_ships if random
#   end
#
#
#
#
# end
class Board

  attr_reader :grid

  def initialize(grid = Board.default_grid)
    @grid = grid

  end

  def self.default_grid
   Array.new(10){Array.new(10)}
  end

  def count
    @grid.flatten.count(:s)
  end

  def [](pos)
    x,y = pos
    mark = @grid[x][y]
  end

  def []= (pos,mark)
    x,y = pos
    @grid[x][y] = mark
  end

  def empty?(pos = nil )
    if pos
    x = pos[0]
    y = pos[1]
   return true if  @grid[x][y].nil?
    end
    if self.count > 0
      false
    else
      true
    end
  end

  def full?
    return false if @grid.flatten.any? { |el| el==nil}
    true
  end

  def place_random_ship
    raise "full board" if full?
    pos = random_pos

    until empty?(pos)
      pos = random_pos
    end

    self[pos] = :s
  end

  def randomize_ships(count = 10)
    count.times { place_random_ship }
  end

  def random_pos
    [rand(@grid.length), rand(@grid.length)]
  end

  def won?
    return false if @grid.flatten.any? { |el| el == :s}
    true
  end


end
