
require_relative "board.rb"
require_relative "player.rb"
require 'byebug'
class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new, board = Board.new)

    @board = board
    @player = player


  end

  def count
    board.count
  end

  def attack(pos)
    if board[pos] == :s
      @board[pos] = :x
      puts "BAM! You hit a ship!"
    else
      @board[pos] = :x
    end
  end

  def game_over?
    @board.won?
  end

  def play_turn
    p "What would you like to move "
    x = @player.get_play
    attack(x)
  end


end
